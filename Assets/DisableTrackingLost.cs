﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class DisableTrackingLost : DefaultTrackableEventHandler {

    // Remove on tracking lost
    protected override void OnTrackingLost() {
        // base.OnTrackingLost();
        // extra behaviour here
    }
}
