﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour {

    public bool enable_rays = true;
    public bool touchMode = true;
    public GameObject gameobj;
    private bool selected = false;
    private GameObject UI;


    // Use this for initialization
    void Start () {
        UI = GameObject.FindGameObjectWithTag("UI");
    }

    void Update() {
        if (enable_rays) {
            // Bit shift the index of the layer (8) to get a bit mask
            int layerMask = 1 << 8;

            // This would cast rays only against colliders in layer 8.
            // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
            // LAYERS: 0 default, 1 TRANSPARENT FX, 2 IGNORE RAYCAST, 4 WATER, 5 UI, 8 POSTPROCESSING.
            layerMask = ~layerMask;

            RaycastHit hit;
            Ray ray = new Ray(transform.position, transform.TransformDirection(Vector3.forward));
            // Does the ray intersect any objects excluding the player layer
            if (touchMode) {
                if (Input.touchCount > 0) {
                    foreach (Touch t in Input.touches) {
                        ray = Camera.main.ScreenPointToRay(Input.GetTouch(t.fingerId).position);
                        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask) && !selected) {
                            gameobj = hit.transform.gameObject;

                            UI.SendMessage("ObjectSelected");
                            gameobj.SendMessage("HitByRay");
                            selected = true;
                        }
                    }
                }
            } else {
                // VIEWPOINT RAYCAST; NOT USED
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask) && !selected) {
                    gameobj = hit.transform.gameObject;
                    UI.SendMessage("ObjectSelected");
                    gameobj.SendMessage("HitByRay");
                    selected = true;
                }
            }
        }       
    }

    public void Deselect() {
        gameobj.SendMessage("NotHitByRay");
        gameobj = null;
        selected = false;
    }

    public void TriggerEvent(string event_name, string name) {
        gameobj.SendMessage(event_name, name);
    }
    public void ChangeTexture(Color color) {
        gameobj.SendMessage("ChangeTexture", color);
    }
}
