﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_Manager : MonoBehaviour {

    public GameObject[] canvases;
    public GameObject arcamera;
    public GameObject bgcanvas;
    public GameObject scale_btnprefab;
    public GameObject scale_grid;
    public GameObject texture_btnprefab;
    public GameObject texture_grid;
    private GameObject selected; //Contains the selected GameObject
    private Raycaster raycaster;

    private void Start() {
        DisableCanvas();
        raycaster = arcamera.GetComponent<Raycaster>();
    }

    private void ObjectSelected() {
        Debug.Log("Object Selected");
        selected = arcamera.GetComponent<Raycaster>().gameobj;

        if (selected) {
            EnableCanvas("Selection Canvas");
        } else {
            DisableCanvas();
        }
    }

    public void EnableCanvas(string name)
    {
        Object[] meshes;
        switch (name) {
            case "Rotation Canvas":
                selected.gameObject.SendMessage("RotateMode");
                break;
            case "Resize Canvas":
                selected.gameObject.SendMessage("ScaleMode");
                break;
            case "Selection Canvas":
                selected.gameObject.SendMessage("NormalMode");
                break;
            case "Replace Canvas":
                // Removing previous objects
                foreach (Transform child in scale_grid.transform) {
                    GameObject.Destroy(child.gameObject);
                }
                // Creating new childs
                meshes = Resources.LoadAll("Meshes/" + selected.tag + "/Prefabs", typeof(GameObject));
                foreach (var mesh in meshes) {
                    print(mesh);
                    GameObject btn = Instantiate(scale_btnprefab, scale_grid.transform);
                    btn.GetComponent<Image>().material = Resources.Load("Meshes/" + selected.tag + "/Images/" + mesh.name, typeof(Material)) as Material; // Getting the asociated material
                    btn.GetComponent<Button>().onClick.AddListener(() => {
                        raycaster.TriggerEvent("ChangeModel", selected.tag + "/Prefabs/" + mesh.name);
                        raycaster.TriggerEvent("SetTag", selected.tag);
                        raycaster.TriggerEvent("SetName",mesh.name);
                    });
                }
                            
                break;
            case "Texture Canvas":
                // Removing previous objects
                foreach (Transform child in texture_grid.transform) {
                    GameObject.Destroy(child.gameObject);
                }
                
                // Creating new childs
                meshes = Resources.LoadAll("Textures/Images/", typeof(Material));
                foreach (Material material in meshes) {
                    Debug.Log(material);
                    GameObject btn = Instantiate(texture_btnprefab, texture_grid.transform);
                    btn.GetComponent<Image>().material = Resources.Load("Textures/Images/" + material.name, typeof(Material)) as Material; ; // Getting the asociated material
                    btn.GetComponent<Button>().onClick.AddListener(() => {
                        raycaster.ChangeTexture(material.GetColor("_Color"));
                    });
                }
                break;
        }

        foreach (GameObject canvas in canvases)
        {
            if (canvas.name == name) canvas.SetActive(true);
            else canvas.SetActive(false);
        }
        bgcanvas.SetActive(true);
    }

    public void SetBgCanvas(bool active) {
        bgcanvas.SetActive(active);
    }

    void DisableCanvas() {
        foreach (GameObject canvas in canvases) {
            canvas.SetActive(false);
        }
        bgcanvas.SetActive(false);
    }

    public void BackgroundTouch() {
        arcamera.SendMessage("Deselect");
        DisableCanvas();
    }

    
}

