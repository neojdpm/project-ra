﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ObjectController : MonoBehaviour {
    public bool scalemode  = false;
    public bool rotatemode = false;
    public string current_tag; // Current path of the object selected
    public string current_name; // Current path of the object selected

    public Vector2 startPos;
    public Vector2 direction;

    private Vector3 scalevector; // Previous scale size of the object

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update() {
        
        if (Input.touchCount > 0) {
            if (rotatemode) {
                Debug.Log("ROTATE MODE");
                transform.Rotate(Vector3.up * Input.GetTouch(0).deltaPosition.x);
            } else if (scalemode) {
                Debug.Log("SCALE MODE");
                Touch touch = Input.GetTouch(0);

                // Handle finger movements based on TouchPhase
                switch (touch.phase) {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        // Record initial touch position.
                        startPos = touch.position;
                        break;

                    //Determine if the touch is a moving touch
                    case TouchPhase.Moved:
                        // Determine direction by comparing the current touch position with the initial one
                        direction = touch.position - startPos;
                        break;

                    case TouchPhase.Ended:
                        // Report that the touch has ended when it ends
                        //scalevector = transform.localScale;
                        break;
                }
                //transform.localScale = scalevector * 2;
                transform.localScale = scalevector * direction.y * 0.01f;
            }
        }
    }

    // Update is called once per frame
    public void HitByRay() {
        scalevector = transform.localScale;
    }

    // Update is called once per frame
    public void NotHitByRay() {
        NormalMode();
    }

    public void RotateMode() {
        rotatemode = true;
    }

    public void ScaleMode() {
        scalemode = true;
    }

    public void NormalMode() {
        rotatemode = false;
        scalemode = false;
    }

    public void SetTag(string tag) {
        current_tag = tag;
    }

    public void SetName(string name) {
        current_name = name;
    }

    public void ChangeModel(string model_name) {
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
        Instantiate(Resources.Load("Meshes/" + model_name, typeof(GameObject)), transform);
    }

    public void ChangeTexture(Color color) {
        Material material = Resources.Load("Meshes/"+ current_tag + "/Materials/" + current_name, typeof(Material)) as Material;
        material.SetColor("_Color", color);
    }

    private Component CopyComponent(Component original) {
        System.Type type = original.GetType();
        Component copy = gameObject.AddComponent(type);
        // Copied fields can be restricted with BindingFlags
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields) {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy;
    }
}
